import java.util.Scanner;

public class Fibonacci {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        int n1 = 0, n2 = 1, n3, q;
        System.out.print(n1 + " " + n2);//printing 0 and 1

        for (q = 2; q < count; ++q)//loop starts from 2 because 0 and 1 are already printed
        {
            n3 = n1 + n2;
            System.out.print(" " + n3);
            n1 = n2;
            n2 = n3;
        }

    }
}