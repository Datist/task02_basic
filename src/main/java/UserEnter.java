import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class UserEnter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write a number:");

        if (sc.hasNextInt()) {
            int number1 = sc.nextInt();
            int number2 = sc.nextInt();
            // int[] a1 = IntStream.range(number1, number2 + 1).toArray();
            List<Integer> a1 = IntStream.range(number1, number2 + 1).boxed().collect(Collectors.toList());
            List<Integer> even = a1
                    .stream()
                    .filter(i -> i % 2 == 0)
                    .collect(Collectors.toList());
            List<Integer> odd = a1
                    .stream()
                    .filter(i -> i % 2 != 0)
                    .collect(Collectors.toList());
            int evenSum = even.stream().mapToInt(Integer::intValue).sum();
            int oddSum = odd.stream().mapToInt(Integer::intValue).sum();
            List<Integer> fibb = new ArrayList<>();
//            UserEnter.print(a1, odd, even, oddSum, evenSum);
            a1 = a1
                    .stream()
                    .sorted(Comparator.reverseOrder())
                    .collect(Collectors.toList());
            even = even
                    .stream()
                    .sorted(Comparator.reverseOrder())
                    .collect(Collectors.toList());
            odd = odd
                    .stream()
                    .sorted(Comparator.reverseOrder())
                    .collect(Collectors.toList());
//
//            int evenSum = even.stream().mapToInt(Integer::intValue).sum();
//            int oddSum = odd.stream().mapToInt(Integer::intValue).sum();
//            System.out.println("evenSum" + evenSum);
//            System.out.println("oddSum" + oddSum);
            Integer count = sc.nextInt();
            Integer n1 = 0, n2 = 1, n3, q;
            fibb.add(n1,n2);

            for (q = 2; q < count; ++q)//loop starts from 2 because 0 and 1 are already printed
            {
                n3 = n1 + n2;
                fibb.add(n3);
                n1 = n2;
                n2 = n3;
            }

            List<Integer> evenfibb = fibb
                    .stream()
                    .filter(i -> i % 2 == 0)
                    .collect(Collectors.toList());
            List<Integer> oddfibb = fibb
                    .stream()
                    .filter(i -> i % 2 != 0)
                    .collect(Collectors.toList());
            int biggestevenfib = evenfibb.stream().mapToInt(Integer::intValue).max().getAsInt();
            int biggestoddfib = oddfibb.stream().mapToInt(Integer::intValue).max().getAsInt();

            UserEnter.print(a1, odd, even, oddSum, evenSum, fibb, oddfibb, evenfibb, biggestevenfib, biggestoddfib );
        }


//                System.out.println(
//                        a1
//                                .map(Objects::toString)
//                                .collect(Collectors.joining(", ", "[", "]"))
//                );
//                System.out.println("The Odd Numbers are:");
//                System.out.println(
//                        odd
//                                .map(Objects::toString)
//                                .collect(Collectors.joining(" "))
//                );
//                for (int i = number1; i <= number2; i++) {
//                    if (i % 2 != 0) {
//                        System.out.print(i + " ");
//                    }
//                }
//                System.out.println("The Even Numbers are:");
//                System.out.println(
//                        even
//                                .map(Objects::toString)
//                                .collect(Collectors.joining(" "))
//                );
//
//                System.out.println(
//                        even
//                                .map(Objects::toString)
//                                .collect(Collectors.joining(" "))
//                );
//                System.out.println("Even Numbers from " + number1 + " to " + number2 + " are: ");
//                for (int x = number1; x <= number2; x++) {
//                    //if number%2 == 0 it means its an even number
//                    if (x % 2 == 0) {
//                        System.out.print(x + " ");
//
//                    }
//                    String[] reverseArray2 = {(x + " ")};
//                    System.out.print("\narray reverted ");
//                    for (int z = reverseArray2.length - 1; z >= 0; z--) {
//                        System.out.print(reverseArray2[z] + " ");
//
//
//                    }
//                }
//            }
//        }
    }

    static void print(List<Integer> all, List<Integer> odd, List<Integer> even,
                      int oddSum, int evenSum, List<Integer> fibb, List<Integer> oddfibb, List<Integer> evenfibb,
                      int biggestevenfib, int biggestoddfib) {
        System.out.println("All Numbers are:");
        System.out.println(
                all
                        .stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(" "))
        );
        System.out.println("The Even Numbers are:");
        System.out.println(
                even
                        .stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(" "))
        );
        System.out.println("The Odd Numbers are:");
        System.out.println(
                odd
                        .stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(" "))
        );
        System.out.println("Odd numbers sum");
        System.out.println(
                oddSum
        );
        System.out.println("Even numbers sum:");
        System.out.println(
                evenSum
        );
        System.out.println("Fibonacci array is:");
        System.out.println(
                fibb
        );
        System.out.println("The Even Fibonacci Numbers are:");
        System.out.println(
                evenfibb
                        .stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(" "))
        );
        System.out.println("The Odd Fibonacci Numbers are:");
        System.out.println(
                oddfibb
                        .stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(" "))
        );

        System.out.println("Bigest Odd number");
        System.out.println(
                biggestoddfib
        );
        System.out.println("Bigest even number:");
        System.out.println(
                biggestevenfib
        );
    }

}